import React from 'react'


//Create a Context Object
//A context object as the name states is a data type of an object that can be used to store information that can be shared to other components within the app.
//Context object is a different approach to passing information between components and allows easier access by avoiding the use of prop-drilling.
const UserContext = React.createContext()

//The "Provider" component allows the other components to consume.use the context object and supply the necessary information needed to the context object
export const UserProvider = UserContext.Provider

export default UserContext



//useState
/*
	initial value of empty/0/false/true
	setter - to set the new value of getter
*/


//useEffect
/*
	if there are changes in the component, or display of the component. Then it will run the function

	*dependency array is needed [value1, value2]
*/


//useContext
/*
	UserProvider(App.js) - source of data that will make accessable for all path/files
	
	useContext - global value that can be used to files
*/
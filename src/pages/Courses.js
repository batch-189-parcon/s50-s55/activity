import {Fragment, useEffect, useState} from 'react'
// import coursesData from '../data/coursesData';
import CourseCard from '../components/CourseCard';

export default function Courses () {

	// console.log(coursesData)
	// console.log(coursesData[0])

	const [courses, setCourses] = useState([])

	useEffect(() => {
		fetch('http://localhost:4000/courses/')
		.then(response => response.json())
		.then(data => {

			console.log(data)

			setCourses(data.map(course => {
				return (

						<CourseCard key={course._id} courseProp={course} />
					)
			}))
		})

	}, [])

	// const courses = coursesData.map(course => {
	// 	return(
	// 			//key is a special attribute
	// 			//unique with other elements
	// 			<CourseCard key={course.id} courseProp={course}/>
	// 		)
	// })

	return (

		//Parent component
		<Fragment>
			<h1>Courses</h1>
			{courses}
		</Fragment>




		)


}
import {Row, Col,} from 'react-bootstrap';
import { Link } from 'react-router-dom'
import {Nav} from 'react-bootstrap'

export default function Banner () {
	




	return(

			<Row>
				<Col className="p-5">
				
					<h1>404 Page Not Found</h1>
					<p>Cannot access url page.</p>
					
					<Nav.Link as={ Link } to ="/">Go back to home page.</Nav.Link>
				
				</Col>
			</Row>
		)
}